function validateForm(email, password, passwordVerify, bbo) {
    return validateEmail(email) && validatePassword(password, passwordVerify) && validateBBO(bbo);
}

function validatePassword(password, passwordVerify) {
    return password === passwordVerify;
}

function validateUsername(username) {
    var regex = /^[a-zA-Z0-9]+$/;
    return regex.test(username);
}

function validateBBO(BBO) {
    var regex = /^\d+$/;
    return BBO.length == 6 && regex.test(BBO)
}

function validateEmail(email) {
    var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return regex.test(email);
}